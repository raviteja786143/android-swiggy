package com.android.slots.ui.activity

import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import com.android.slots.R
import com.android.slots.di.DataManager
import com.android.slots.viewmodel.BaseViewModel
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject


abstract class BaseActivity<T : ViewDataBinding, V : BaseViewModel> : AppCompatActivity(), HasSupportFragmentInjector {

    @Inject
    lateinit var dataManager: DataManager

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var binding: T

    var viewModel: V? = null

    override fun supportFragmentInjector() = dispatchingAndroidInjector

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = getMyViewModel()
        binding = DataBindingUtil.setContentView(this, getLayoutId())
        binding.executePendingBindings()
    }

    override fun onBackPressed() {
        when (Navigation.findNavController(this, R.id.nav_host_fragment)//exit app from home frag not to splash frag
                .currentDestination?.id) {
            R.id.slotsFragment -> finishAffinity()
        }
        super.onBackPressed()
    }

    @LayoutRes
    abstract fun getLayoutId(): Int

    abstract fun getMyViewModel(): V

}
