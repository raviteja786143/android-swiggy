package com.android.slots.ui.fragment

import android.graphics.drawable.NinePatchDrawable
import android.os.Bundle
import android.os.Parcelable
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.slots.R
import com.android.slots.adapter.ExpandableAdapter
import com.android.slots.api.VariantsModel
import com.android.slots.databinding.SlotsFragmentBinding
import com.android.slots.viewmodel.SlotsViewModel
import com.android.slots.vo.Status
import com.h6ah4i.android.widget.advrecyclerview.animator.RefactoredDefaultItemAnimator
import com.h6ah4i.android.widget.advrecyclerview.decoration.ItemShadowDecorator
import com.h6ah4i.android.widget.advrecyclerview.decoration.SimpleListDividerDecorator
import com.h6ah4i.android.widget.advrecyclerview.expandable.RecyclerViewExpandableItemManager
import com.h6ah4i.android.widget.advrecyclerview.utils.WrapperAdapterUtils
import com.orhanobut.logger.Logger


class SlotsFragment : BaseFragment<SlotsFragmentBinding, SlotsViewModel>(), RecyclerViewExpandableItemManager.OnGroupExpandListener, RecyclerViewExpandableItemManager.OnGroupCollapseListener {

    private var myItemAdapter: ExpandableAdapter? = null

    private var vairants: VariantsModel = VariantsModel(arrayListOf(), arrayListOf())

    private var mLayoutManager: RecyclerView.LayoutManager? = null

    private var mRecyclerViewExpandableItemManager: RecyclerViewExpandableItemManager? = null

    private var mWrappedAdapter: RecyclerView.Adapter<*>? = null

    companion object {
        const val SAVED_STATE_EXPANDABLE_ITEM_MANAGER = "SAVED_STATE_EXPANDABLE_ITEM_MANAGER"
        fun newInstance() = SlotsFragment()
    }

    override fun getLayoutId(): Int {
        return R.layout.slots_fragment
    }

    override fun getMyViewModel(): SlotsViewModel = ViewModelProviders.of(this, viewModelFactory).get(SlotsViewModel::class.java)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val eimSavedState = savedInstanceState?.getParcelable<Parcelable>(SAVED_STATE_EXPANDABLE_ITEM_MANAGER)
        mRecyclerViewExpandableItemManager = RecyclerViewExpandableItemManager(eimSavedState)
        mLayoutManager = LinearLayoutManager(context)
        val animator = RefactoredDefaultItemAnimator()
        mRecyclerViewExpandableItemManager?.setOnGroupExpandListener(this)
        mRecyclerViewExpandableItemManager?.setOnGroupCollapseListener(this)
        myItemAdapter = ExpandableAdapter(vairants, dataBindingComponent, getHelper())
        mWrappedAdapter = mRecyclerViewExpandableItemManager?.createWrappedAdapter(myItemAdapter!!)
        animator.supportsChangeAnimations = false
        binding.recyclerView.layoutManager = mLayoutManager
        binding.recyclerView.adapter = mWrappedAdapter  // requires *wrapped* adapter
        binding.recyclerView.itemAnimator = animator
        binding.recyclerView.setHasFixedSize(true)
        binding.recyclerView.addItemDecoration(ItemShadowDecorator(ContextCompat.getDrawable(context!!, R.drawable.material_shadow_z1) as NinePatchDrawable))
        binding.recyclerView.addItemDecoration(SimpleListDividerDecorator(ContextCompat.getDrawable(context!!, R.drawable.list_divider), true))
        mRecyclerViewExpandableItemManager?.attachRecyclerView(binding.recyclerView)
        viewModel?.slotsRequestMLD?.value = true
        viewModel?.slotsResult?.observe(this, Observer {
            Logger.e("RESP $it")
            if (it.status == Status.LOADING) {
                binding.progressBar.visibility = View.VISIBLE
            } else {
                binding.progressBar.visibility = View.INVISIBLE
            }
            if (it.status == Status.SUCCESS && it.data != null && it.data.variant != null) {
                vairants.variantGroups.clear().apply { vairants.variantGroups.addAll(it.data.variant.variantGroups) }
                vairants.excludeList.clear().apply { vairants.excludeList.addAll(it.data.variant.excludeList) }
                binding.recyclerView.post {
                    myItemAdapter?.notifyDataSetChanged()
                    mWrappedAdapter?.notifyDataSetChanged()
                }
            }
            if (it.status == Status.ERROR) {
                Toast.makeText(context!!, R.string.some_went_wrong, Toast.LENGTH_SHORT).show()
            }
        })
    }

    override fun onGroupCollapse(groupPosition: Int, fromUser: Boolean, payload: Any?) {
        if (fromUser) {
            vairants.variantGroups[groupPosition] = vairants.variantGroups[groupPosition].apply { isExpand = false }
            mWrappedAdapter?.notifyItemChanged(groupPosition)
        }
    }

    override fun onGroupExpand(groupPosition: Int, fromUser: Boolean, payload: Any?) {
        if (fromUser) {
            adjustScrollPositionOnGroupExpanded(groupPosition)
            vairants.variantGroups[groupPosition] = vairants.variantGroups[groupPosition].apply { isExpand = true }
            mWrappedAdapter?.notifyItemChanged(groupPosition)
        }
    }

    private fun adjustScrollPositionOnGroupExpanded(groupPosition: Int) {
        val childItemHeight = activity?.resources?.getDimensionPixelSize(R.dimen.list_item_height)
        val topMargin = (activity?.resources?.displayMetrics?.density?.times(16))?.toInt() // top-spacing: 16dp
        if (childItemHeight != null && topMargin != null) {
            mRecyclerViewExpandableItemManager?.scrollToGroup(groupPosition, childItemHeight, topMargin, topMargin)
        }
    }

    override fun onDestroyView() {
        if (mRecyclerViewExpandableItemManager != null) {
            mRecyclerViewExpandableItemManager?.release()
            mRecyclerViewExpandableItemManager = null
        }
        binding.recyclerView.itemAnimator = null
        binding.recyclerView.adapter = null
        if (mWrappedAdapter != null) {
            WrapperAdapterUtils.releaseAll(mWrappedAdapter)
            mWrappedAdapter = null
        }
        mLayoutManager = null
        arguments?.clear()
        super.onDestroyView()
    }


}

