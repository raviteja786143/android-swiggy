package com.android.slots.ui.fragment

import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.annotation.NonNull
import androidx.databinding.DataBindingComponent
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import com.android.slots.BR
import com.android.slots.binding.FragmentDataBindingComponent
import com.android.slots.binding.Helper
import com.android.slots.di.DataManager
import com.android.slots.di.Injectable
import com.android.slots.ui.activity.MainActivity
import com.android.slots.viewmodel.BaseViewModel
import fr.castorflex.android.smoothprogressbar.SmoothProgressBar
import javax.inject.Inject


abstract class BaseFragment<T : ViewDataBinding, V : BaseViewModel> : Fragment(), Injectable {

    lateinit var binding: T

    var viewModel: V? = null

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var dataManager: DataManager

    @Inject
    lateinit var mainActivity: MainActivity

    var dataBindingComponent: DataBindingComponent = FragmentDataBindingComponent(this)

    var progressBar: SmoothProgressBar? = null

    /*override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        onViewStateRestored(savedInstanceState)
        retainInstance = true
    }*/

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, getLayoutId(),
                container, false, dataBindingComponent)
        initViewModel()
        return binding.root
    }

    private fun initViewModel() {
        viewModel = getMyViewModel()
        viewModel?.helper = getHelper()
        binding.setVariable(BR.viewModel, viewModel)
    }

    @NonNull
    fun getHelper(): Helper {

        return object : Helper {

            override fun showMessage(message: String) {
                if (TextUtils.isEmpty(message)) return
                Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
            }

            override fun showMessage(message: Int) {
                Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
            }

            override fun showProgress(flag: Boolean) {
                if (progressBar != null) {
                    if (flag) {
                        progressBar?.visibility = View.VISIBLE
                    } else {
                        progressBar?.visibility = View.GONE
                    }
                }
            }

            override fun onBackPressed() {
                activity!!.onBackPressed()
            }

            override fun navigate(id: Int) {
                navController().navigate(id)
            }

            override fun navigate(id: Int, bundle: Bundle) {
                navController().navigate(id, bundle)
            }
        }
    }

    @LayoutRes
    abstract fun getLayoutId(): Int

    abstract fun getMyViewModel(): V

    fun navController() = NavHostFragment.findNavController(this)

}
