package com.android.slots.ui.activity

import androidx.lifecycle.ViewModelProviders
import com.android.slots.R
import com.android.slots.databinding.MainActivityBinding
import com.android.slots.viewmodel.MainActivityViewModel

class MainActivity : BaseActivity<MainActivityBinding, MainActivityViewModel>() {

    override fun getLayoutId(): Int {
        return R.layout.main_activity
    }

    override fun getMyViewModel(): MainActivityViewModel = ViewModelProviders.of(this, viewModelFactory).get(MainActivityViewModel::class.java)

}
