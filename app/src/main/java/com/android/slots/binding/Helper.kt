package com.android.slots.binding

import android.os.Bundle

interface Helper {

    fun showMessage(message: String)

    fun showMessage(message: Int)

    fun showProgress(flag: Boolean)

    fun navigate(id: Int)

    fun navigate(id: Int, bundle: Bundle)

    fun onBackPressed()

}
