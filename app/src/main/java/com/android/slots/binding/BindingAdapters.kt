/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.slots.binding

import android.text.TextUtils
import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.android.slots.R
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

/**
 * Data Binding adapters specific to the app.
 */
object BindingAdapters {

    @JvmStatic
    @BindingAdapter("visibleGone")
    fun showHide(view: View, show: Boolean) {
        view.visibility = if (show) View.VISIBLE else View.INVISIBLE
    }

    @JvmStatic
    @BindingAdapter("loadUrl")
    fun loadUrl(view: ImageView?, url: String) {
        if (view == null || view.context == null) return
        if (TextUtils.isEmpty(url)) {//if no profile image load default user drawable
            Glide.with(view.context)
                    .load(R.drawable.custom_circle_white)
                    .into(view)
        } else {
            Glide.with(view.context)
                    .load(url)
                    .apply(RequestOptions().placeholder(R.drawable.custom_circle_white).error(R.drawable.custom_circle_white).skipMemoryCache(true))
                    .into(view)
        }
    }

    @JvmStatic
    @BindingAdapter("loadBg")
    fun loadBg(view: View?, isGrey: Boolean) {
        if (view == null || view.context == null) return
        if (isGrey) {
            view.setBackgroundResource(R.color.grey)
        } else {
            view.setBackgroundResource(R.color.white)
        }
    }

    @JvmStatic
    @BindingAdapter("expand")
    fun expand(view: View?, isExpand: Boolean) {
        if (view == null || view.context == null) return
        if (isExpand) {
            view.setBackgroundResource(R.drawable.ic_red_up)
        } else {
            view.setBackgroundResource(R.drawable.ic_red_down)
        }
    }

}
