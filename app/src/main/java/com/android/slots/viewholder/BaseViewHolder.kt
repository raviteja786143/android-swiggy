package com.android.slots.viewholder

import android.view.View
import androidx.databinding.DataBindingComponent
import androidx.databinding.ViewDataBinding
import com.android.slots.BR
import com.android.slots.viewmodel.BaseViewModel
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractExpandableItemViewHolder

abstract class BaseViewHolder<T : ViewDataBinding, V : BaseViewModel>(val itemView: View, val binding: T, var viewModel: V, val dataBindingComponent: DataBindingComponent?) : AbstractExpandableItemViewHolder(itemView) {

    init {
        binding.setVariable(BR.viewModel, viewModel)
    }

}