package com.android.slots.viewholder

import android.view.View
import androidx.databinding.DataBindingComponent
import com.android.slots.databinding.RowChildBinding
import com.android.slots.viewmodel.ChildViewModel

class ChildViewHolder(itemView: View, val viewType: Int, binding: RowChildBinding, viewModel: ChildViewModel, dataBindingComponent: DataBindingComponent) : BaseViewHolder<RowChildBinding, ChildViewModel>(itemView, binding, viewModel, dataBindingComponent)