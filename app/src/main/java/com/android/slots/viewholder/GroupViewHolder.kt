package com.android.slots.viewholder

import android.view.View
import androidx.databinding.DataBindingComponent
import com.android.slots.databinding.RowGroupBinding
import com.android.slots.viewmodel.GroupViewModel

class GroupViewHolder(itemView: View, val viewType: Int, binding: RowGroupBinding, viewModel: GroupViewModel, dataBindingComponent: DataBindingComponent) : BaseViewHolder<RowGroupBinding, GroupViewModel>(itemView, binding, viewModel, dataBindingComponent)