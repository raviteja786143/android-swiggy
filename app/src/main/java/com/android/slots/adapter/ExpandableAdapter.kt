package com.android.slots.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingComponent
import androidx.databinding.DataBindingUtil
import com.android.slots.R
import com.android.slots.api.VariantsModel
import com.android.slots.binding.Helper
import com.android.slots.databinding.RowChildBinding
import com.android.slots.databinding.RowGroupBinding
import com.android.slots.viewholder.ChildViewHolder
import com.android.slots.viewholder.GroupViewHolder
import com.android.slots.viewmodel.ChildViewModel
import com.android.slots.viewmodel.GroupViewModel
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractExpandableItemAdapter
import com.orhanobut.logger.Logger


class ExpandableAdapter(private var vairants: VariantsModel, private val dataBindingComponent: DataBindingComponent, private val mHelper: Helper) : AbstractExpandableItemAdapter<GroupViewHolder, ChildViewHolder>() {


    private var inflater: LayoutInflater?
    private var selectedIDList: HashMap<String, String> = hashMapOf()

    init {
        setHasStableIds(true)
        inflater = LayoutInflater.from(dataBindingComponent.fragmentBindingAdapters.fragment.context)
    }

    override fun onCreateGroupViewHolder(parent: ViewGroup?, viewType: Int): GroupViewHolder {
        val groupBinding = inflater?.inflate(R.layout.row_group, parent, false)?.let { DataBindingUtil.bind<RowGroupBinding>(it) }
        return GroupViewHolder(groupBinding!!.root, viewType, groupBinding, GroupViewModel().apply { helper = mHelper }, dataBindingComponent)
    }

    override fun onCreateChildViewHolder(parent: ViewGroup?, viewType: Int): ChildViewHolder {
        val childBinding = inflater?.inflate(R.layout.row_child, parent, false)?.let { DataBindingUtil.bind<RowChildBinding>(it) }
        return ChildViewHolder(childBinding!!.root, viewType, childBinding, ChildViewModel().apply { helper = mHelper }, dataBindingComponent)
    }

    override fun onBindGroupViewHolder(groupViewHolder: GroupViewHolder?, groupPosition: Int, viewType: Int) {
        val groupData = vairants.variantGroups[groupPosition]
        groupViewHolder?.viewModel?.groupTitle?.value = groupData.name
        groupViewHolder?.viewModel?.isExpand?.set(groupData.isExpand)
    }

    override fun onBindChildViewHolder(childViewHolder: ChildViewHolder?, groupPosition: Int, childPosition: Int, viewType: Int) {
        val groupData = vairants.variantGroups[groupPosition]
        val childData = groupData.variations[childPosition]
        //val key = "${groupData.groupID}-${childData.id}"
        val key = groupData.groupID
        val mainKey = "${groupData.groupID}-${childData.id}"
        Logger.e("OBCV gp:- $groupPosition cp:- $childPosition cd:- $childData gd:- $groupData")
        childViewHolder?.viewModel?.name?.value = childData.name
        childViewHolder?.viewModel?.price?.value = childData.price.toString()
        childViewHolder?.viewModel?.inStock?.value = childData.inStock.toString()
        childData.isSelected = selectedIDList[key] == childData.id
        childViewHolder?.viewModel?.isSelected?.value = childData.isSelected
        childViewHolder?.binding?.radioButton?.isChecked = childData.isSelected
        childViewHolder?.binding?.radioButton?.setOnClickListener {
            if (!childViewHolder.binding.radioButton.isChecked) {//if not selected remove from list
                childViewHolder.binding.radioButton.isChecked = false
                selectedIDList.remove(key)
                childData.isSelected = false
                vairants.variantGroups[groupPosition].variations[childPosition] = childData
            } else {//if selected add to list
                Logger.e("$mainKey ${vairants.excludeList} $selectedIDList")
                var isSelectable = true
                for (exc1 in vairants.excludeList) {
                    var counter = 0
                    for (exc2 in exc1) {
                        if (exc2.toString() == mainKey) {
                            counter++
                        }
                        for ((selKey, selVal) in selectedIDList) {
                            if (exc2.toString() == "$selKey-$selVal") {
                                counter++
                            }
                        }
                        if (counter == exc1.size) {
                            isSelectable = false
                            break
                        }
                    }
                }
                if (isSelectable) {
                    childViewHolder.binding.radioButton.isChecked = true
                    selectedIDList[key] = childData.id
                    childData.isSelected = true
                    vairants.variantGroups[groupPosition].variations[childPosition] = childData
                } else {
                    mHelper.showMessage("This variant is unavailable")
                }
            }
            notifyDataSetChanged()
        }
    }

    override fun getGroupId(groupPosition: Int): Long {
        return groupPosition.toLong()
    }

    override fun getChildId(groupPosition: Int, childPosition: Int): Long {
        return vairants.variantGroups[groupPosition].variations[childPosition].id.toLong()
    }

    override fun getGroupCount(): Int {
        return vairants.variantGroups.size
    }

    override fun getChildCount(groupPosition: Int): Int {
        return vairants.variantGroups[groupPosition].variations.size
    }

    override fun onCheckCanExpandOrCollapseGroup(groupViewHolder: GroupViewHolder?, groupPosition: Int, x: Int, y: Int, expand: Boolean): Boolean {
        val notEmpty = !vairants.variantGroups[groupPosition].variations.isEmpty()
        if (!notEmpty) {
            dataBindingComponent.fragmentBindingAdapters.showToast(R.string.no_slots_available)
        }
        return notEmpty
    }

}