package com.android.slots.di

import com.android.slots.ui.fragment.SlotsFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuilderModule {

    @ContributesAndroidInjector
    abstract fun contributeSlotsFragment(): SlotsFragment
}