package com.android.slots.di

/**
 * Marks an activity / fragment injectable.
 */
interface Injectable {
    // Empty on purpose
}