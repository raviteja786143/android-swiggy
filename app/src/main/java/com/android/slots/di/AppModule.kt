package com.android.slots.di

import android.content.Context
import com.android.slots.AppDelegate
import com.android.slots.BuildConfig
import com.android.slots.api.SlotsService
import com.android.slots.utils.Constants
import com.android.slots.utils.Constants.CONFIG_TIMEOUT
import com.android.slots.utils.LiveDataCallAdapterFactory
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module(includes = [ViewModelModule::class])
class AppModule {

    @Singleton
    @Provides
    fun provideSlotsService(application: AppDelegate): SlotsService {
        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor { chain ->
            val original = chain.request()
            val builder = original.newBuilder()
                    .method(original.method(), original.body())
            val request = builder.build()
            chain.proceed(request)
        }
        val interceptor = HttpLoggingInterceptor()
        val logLevel = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.BASIC
        interceptor.level = logLevel//BODY->to showMessage logs//NONE->viceversa
        httpClient.addInterceptor(interceptor)
        httpClient.connectTimeout(CONFIG_TIMEOUT, TimeUnit.SECONDS)
        httpClient.readTimeout(CONFIG_TIMEOUT, TimeUnit.SECONDS)
        val client = httpClient.build()
        return Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(LiveDataCallAdapterFactory())
                .client(client)
                .build()
                .create(SlotsService::class.java)
    }

    @Provides
    @Singleton
    fun provideContext(application: AppDelegate): Context = application

    @Provides
    @Singleton
    fun provideDataManager(application: AppDelegate): DataManager {
        return DataManager(application)
    }
}