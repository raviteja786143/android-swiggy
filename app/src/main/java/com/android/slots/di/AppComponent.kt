package com.android.slots.di

import com.android.slots.AppDelegate
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class,
    AndroidSupportInjectionModule::class,
    MainActivityModule::class])
interface AppComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(appDelegate: AppDelegate): Builder

        fun build(): AppComponent
    }

    fun inject(app: AppDelegate)
}