package com.android.slots.api

import com.google.gson.annotations.SerializedName

data class VariationsModel(
        @SerializedName("name") val name: String,
        @SerializedName("price") val price: Int,
        @SerializedName("default") val default: Int,
        @SerializedName("id") val id: String,
        @SerializedName("inStock") val inStock: Int,
        var isSelected: Boolean = false
) : ApiData