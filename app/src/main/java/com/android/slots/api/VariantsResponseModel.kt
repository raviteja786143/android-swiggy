package com.android.slots.api

import com.google.gson.annotations.SerializedName

data class VariantsResponseModel(
        @SerializedName("variants") val variant: VariantsModel?
) : ApiData

