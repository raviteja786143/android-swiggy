package com.android.slots.api

import androidx.lifecycle.LiveData
import retrofit2.http.GET

/**
 * REST API access points
 */
interface SlotsService {

    @GET("bins/3b0u2")
    fun getSlots(): LiveData<ApiResponse<VariantsResponseModel>>
}

