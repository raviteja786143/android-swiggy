package com.android.slots.api

import com.google.gson.annotations.SerializedName

data class VariantGroupModel(
        @SerializedName("group_id") val groupID: String,
        @SerializedName("name") val name: String,
        @SerializedName("variations") val variations: ArrayList<VariationsModel>,
        var isExpand: Boolean = true
) : ApiData

