package com.android.slots.api

import com.google.gson.annotations.SerializedName

data class ExcludeModel(
        @SerializedName("group_id") val groupID: String,
        @SerializedName("variation_id") val variationID: String
) : ApiData {
    override fun toString(): String {
        return "$groupID-$variationID"
    }
}

