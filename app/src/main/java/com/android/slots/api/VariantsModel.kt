package com.android.slots.api

import com.google.gson.annotations.SerializedName

data class VariantsModel(
        @SerializedName("variant_groups") val variantGroups: ArrayList<VariantGroupModel>,
        @SerializedName("exclude_list") val excludeList: ArrayList<ArrayList<ExcludeModel>>,
        val isSelected: Boolean = false
) : ApiData

