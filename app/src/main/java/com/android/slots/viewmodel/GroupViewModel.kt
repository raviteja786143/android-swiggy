package com.android.slots.viewmodel

import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import javax.inject.Inject

class GroupViewModel @Inject constructor() : BaseViewModel() {

    var groupTitle: MutableLiveData<String> = MutableLiveData()

    var isExpand: ObservableField<Boolean> = ObservableField()

}

