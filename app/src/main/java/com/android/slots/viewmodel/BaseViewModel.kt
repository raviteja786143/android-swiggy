package com.android.slots.viewmodel

import android.text.TextUtils
import android.view.View
import androidx.lifecycle.ViewModel
import com.android.slots.R
import com.android.slots.binding.Helper
import com.android.slots.utils.SingleLiveData
import com.android.slots.vo.Resource
import com.android.slots.vo.Status
import retrofit2.HttpException
import java.net.HttpURLConnection.HTTP_UNAUTHORIZED
import java.net.UnknownHostException

abstract class BaseViewModel : ViewModel() {

    val clickLiveData: SingleLiveData<Int> = SingleLiveData()
    var helper: Helper? = null

    fun onClick(view: View) {
        clickLiveData.value = view.id
    }

    fun processResponse(result: Resource<Any>?) {
        if (result == null) {
            helper?.showProgress(false)
            helper!!.showMessage(R.string.some_went_wrong)
        } else if (!TextUtils.isEmpty(result.error.message) && result.status == Status.ERROR) {
            handleError(result.error)
        } else if (result.status == Status.ERROR) {
            helper?.showProgress(false)
            helper!!.showMessage(R.string.some_went_wrong)
        } else if (result.status == Status.SUCCESS) {
            helper?.showProgress(false)
        }
    }

    private fun handleError(e: Throwable?) {
        try {
            helper?.showProgress(false)
            if (e == null) return
            e.printStackTrace()
            if (isHttpStatusCode(e, HTTP_UNAUTHORIZED)) {
                if (!TextUtils.isEmpty(e.message)) {
                    helper!!.showMessage(e.message!!)
                }
            } else {
                if (e is UnknownHostException) {
                    helper!!.showMessage(R.string.network_error)
                } else {
                    if (!TextUtils.isEmpty(e.message)) {
                        helper!!.showMessage(e.message!!)
                    } else {
                        helper!!.showMessage(R.string.req_failed)
                    }
                }
            }
        } catch (e1: Exception) {
            e1.printStackTrace()
        }
    }

    private fun isHttpStatusCode(throwable: Throwable?, statusCode: Int): Boolean {
        return throwable != null && throwable is HttpException && throwable.code() == statusCode
    }

}