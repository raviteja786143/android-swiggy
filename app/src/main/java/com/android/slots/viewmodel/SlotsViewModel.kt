package com.android.slots.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.android.slots.api.VariantsResponseModel
import com.android.slots.repository.SlotsRepository
import com.android.slots.vo.Resource
import javax.inject.Inject

class SlotsViewModel @Inject constructor(private val slotsRepository: SlotsRepository) : BaseViewModel() {

    var slotsRequestMLD: MutableLiveData<Boolean> = MutableLiveData()

    var showProgress: MutableLiveData<Boolean> = MutableLiveData()

    val slotsResult: LiveData<Resource<VariantsResponseModel>> = Transformations.switchMap(slotsRequestMLD) {
        slotsRepository.getSlots()
    }
}

