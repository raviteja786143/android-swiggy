package com.android.slots.viewmodel

import androidx.lifecycle.MutableLiveData
import javax.inject.Inject

class ChildViewModel @Inject constructor() : BaseViewModel() {

    val name: MutableLiveData<String> = MutableLiveData()

    val price: MutableLiveData<String> = MutableLiveData()

    val inStock: MutableLiveData<String> = MutableLiveData()

    val isSelected: MutableLiveData<Boolean> = MutableLiveData()

}

