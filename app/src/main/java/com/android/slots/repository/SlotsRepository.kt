package com.android.slots.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.android.slots.AppExecutors
import com.android.slots.api.SlotsService
import com.android.slots.api.VariantsResponseModel
import com.android.slots.vo.Resource
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SlotsRepository @Inject constructor(appExecutors: AppExecutors, slotsService: SlotsService) : BaseRepository(appExecutors, slotsService) {

    var regResponse: VariantsResponseModel? = null

    fun getSlots(): LiveData<Resource<VariantsResponseModel>> {

        return object : NetworkBoundResource<VariantsResponseModel, VariantsResponseModel>(appExecutors) {



            override fun shouldFetch(data: VariantsResponseModel?): Boolean {
                return true
            }

            override fun saveCallResult(item: VariantsResponseModel) {
                regResponse = item
            }

            override fun loadFromDb(): LiveData<VariantsResponseModel> {
                val mld: MutableLiveData<VariantsResponseModel> = MutableLiveData()
                if (regResponse == null) {
                    regResponse = VariantsResponseModel(null)
                }
                mld.value = regResponse
                return mld
            }

            override fun createCall() = slotsService.getSlots()

        }.asLiveData()
    }


}