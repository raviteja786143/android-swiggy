package com.android.slots.repository

import com.android.slots.AppExecutors
import com.android.slots.api.SlotsService


abstract class BaseRepository(var appExecutors: AppExecutors, var slotsService: SlotsService) {

}