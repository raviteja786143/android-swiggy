package com.android.slots.utils


object Constants {

    const val BASE_URL = "https://api.myjson.com/"
    const val CONFIG_TIMEOUT = 60L //60 sec network call timeouts
    const val GET_SLOTS = "GET_SLOTS"

}